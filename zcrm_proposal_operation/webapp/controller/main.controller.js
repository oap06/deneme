sap.ui.define([
	"./BaseController",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/MessageBox",
	"sap/ui/core/Fragment"
], function (BaseController, Filter, FilterOperator, MessageBox, Fragment) {
	"use strict";

	return BaseController.extend("zcrm.zcrm_proposal_operation.controller.main", {
		onInit: function () {
			BaseController.prototype.onInit.apply(this, arguments);
			this.getView().addStyleClass("sapUiSizeCompact");

		},
		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf zcrm.zcrm_sales_order_operation.view.detail
		 */
		onBeforeRendering: function () {

		},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf zcrm.zcrm_sales_order_operation.view.detail
		 */
		onAfterRendering: function () {
			var oFilters = [];
			var oModel = this.getView().getModel();
			var oDurum = this.getView().byId("idDurum");

			var oFilter = new Filter("ProcessType", FilterOperator.EQ, "ZS05");
			oFilters.push(oFilter);

			oModel.read("/orderStatusSearchHelpSet", {
				filters: oFilters,
				success: function (oData, response) {
					var oModelJsonList = new sap.ui.model.json.JSONModel(oData);
					oDurum.setModel(oModelJsonList, "oDurum");
				},
				error: function (oError) {}
			});
		},
		onFilterSearch: function (oEvent) {

			var oFilters = [];
			var oPropNo = this.getView().byId("inputPropId").getValue();
			if (oPropNo !== "") {
				var oFilter = new Filter("ObjectId", FilterOperator.EQ, oPropNo);
				oFilters.push(oFilter);
			}
			var oDateFrom = this.getOwnerModelProperty("headersearch", "/CredateValue");
			var oDateTo = this.getOwnerModelProperty("headersearch", "/CreDateto");
			if (oDateFrom) {
				var utcDateFrom = new Date(Date.UTC(oDateFrom.getFullYear(), oDateFrom.getMonth(), oDateFrom.getDate()));
				var utcDateTo = new Date(Date.UTC(oDateTo.getFullYear(), oDateTo.getMonth(), oDateTo.getDate()));
				oFilter = new Filter({
					path: "PostingDate", 
					operator: FilterOperator.BT, 
					value1: utcDateFrom, 
					value2: utcDateTo
				});
				oFilters.push(oFilter);
			}

			var oPartner = this.getView().byId("inputPartnerId").getValue();
			if (oPartner !== "") {
				oFilter = new Filter("Partner", FilterOperator.EQ, oPartner);
				oFilters.push(oFilter);
			}

			var aDurum = this.getView().byId("idDurum").getSelectedKey();
			if (aDurum !== "") {
				oFilter = new Filter("Status", FilterOperator.EQ, aDurum);
				oFilters.push(oFilter);
			}

			var oKayitSayi = this.getView().byId("idKayitSayisi").getValue();
			if (oKayitSayi !== "") {
				oFilter = new Filter("MaxRows", FilterOperator.EQ, oKayitSayi);
				oFilters.push(oFilter);
			}

			var oTable = this.getView().byId("ProposalTabId");
			var oModel = oTable.getModel();
			var oModelJsonList = new sap.ui.model.json.JSONModel();

			var oBusyDialog = new sap.m.BusyDialog();
			oBusyDialog.open();

			oModel.read("/salesQuotationSet", {
				filters: oFilters,
				success: function (oData, response) {
					oModelJsonList.setData(oData);
					oTable.setModel(oModelJsonList, "oProposalModel");
					oModelJsonList.setSizeLimit(oKayitSayi);
					oBusyDialog.close();
				},
				error: function (oError) {
					oBusyDialog.close();
				}
			});
		},

		onItemPress: function (oEvent) {
			var oNo = oEvent.getSource().getBindingContext("oProposalModel").getObject().Guid;
			sap.ui.core.UIComponent.getRouterFor(this).navTo("detail", {
				guid: oNo
			});
		},

		toCreate: function () {

			if (sap.ui.getCore().getModel("oModelGlobal")) {
				sap.ui.getCore().setModel(null, "oModelGlobal");
			}
		
			
				this.setOwnerModelProperty("selected", "/Custidname", "");
				this.setOwnerModelProperty("selected", "/Custid", "");

			sap.ui.core.UIComponent.getRouterFor(this).navTo("CreateEdit", {
				guid: "00000000-0000-0000-0000-000000000000",
				orderType: "ZS05"
			});
		},
		// onCustomerValueHelp: function () {

		// 	if (!this._oValueHelpDialog) {
		// 		this._oValueHelpDialog = sap.ui.xmlfragment("zcrm.zcrm_proposal_operation.fragments.CustomerSearch", this);
		// 		this.getView().addDependent(this._oValueHelpDialog);
		// 		this._oValueHelpDialog.open();
		// 	} else {

		// 		this._oValueHelpDialog.open();
		// 	}

		// },

		handleCustCancel: function () {
			this._oValueHelpDialog.close();
		},
		handleCustomerSearch: function () {

			var oModelCust = this.getView().getModel("CUST");

			var oPartner = sap.ui.getCore().byId("idCustIdent").getValue();
			var oAd = sap.ui.getCore().byId("idCustName").getValue();
			var oSoyad = sap.ui.getCore().byId("idCustLastName").getValue();
			var oTel = sap.ui.getCore().byId("idCustTel").getValue();
			var oTur = sap.ui.getCore().byId("idCustType").getSelectedKey();
			var oBusyDialog = new sap.m.BusyDialog();

			var oFilters = [];
			if (oPartner !== "") {
				var oFilter = new Filter("Partner", FilterOperator.EQ, oPartner);
				oFilters.push(oFilter);
			}
			if (oAd !== "") {
				oFilter = new Filter("Firstname", FilterOperator.EQ, oAd);
				oFilters.push(oFilter);
			}
			if (oSoyad !== "") {
				oFilter = new Filter("Lastname", FilterOperator.EQ, oSoyad);
				oFilters.push(oFilter);
			}
			if (oTel !== "") {
				oFilter = new Filter("Telephonemob", FilterOperator.EQ, oTel);
				oFilters.push(oFilter);
			}
			if (oTur !== "") {
				oFilter = new Filter("Partnertype", FilterOperator.EQ, oTur);
				oFilters.push(oFilter);
			}

			var oModelJsonList = new sap.ui.model.json.JSONModel();
			var that = this;
			oBusyDialog.open();
			oModelCust.read("/searchCustomerSet", {
				filters: oFilters,
				success: function (oData, response) {
					oModelJsonList.setData(oData);
					that._oValueHelpDialog.setModel(oModelJsonList, "oCustomer");
					oBusyDialog.close();
				},
				error: function (oError) {
					oBusyDialog.close();
				}
			});

		},

		// handleCustomerListItemPress: function (oEvent) {
		// 	var oInd = oEvent.getParameter("listItem").getBindingContextPath().split('/')[2];
		// 	var oPartner = sap.ui.getCore().byId("CustTabId").getItems()[oInd].getCells()[0].getText();
		// 	this.getView().byId("inputPartnerId").setValue(oPartner);
		// 	this._oValueHelpDialog.close();
		// },

	});
});