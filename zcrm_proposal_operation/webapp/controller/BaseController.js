sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/Dialog",
	"sap/m/NotificationListItem",
	"sap/m/List",
	"sap/m/Button",
	"sap/m/BusyDialog",
	"sap/ui/core/BusyIndicator",
	"sap/m/MessageBox"
], function (Controller, Filter, FilterOperator, Dialog, NotificationListItem, List, Button, BusyDialog, BusyIndicator, MessageBox) {
	"use strict";

	return Controller.extend("zcrm.zcrm_inquire_price.controller.BaseController", {
		onInit: function () {

			this.createActionResultDialog();

		},
		getOwnerModelProperty: function (oModel, sPath) {
			return this.getOwnerComponent().getModel(oModel).getProperty(sPath);
		},
		setOwnerModelProperty: function (oModel, sPath, value) {
			return this.getOwnerComponent().getModel(oModel).setProperty(sPath, value);
		},

		/**
		 * this.BaseValueHelp("/valueHelpSet", "KatalogModel", {
		 *		Field1: sCode, => ["Field1", sCode]
		 *		Field2: 234,
		 *		customFilter: {path: 'Field3', value1: 'hello', operator: FilterOperator.LT}
		 * });
		 */
		BaseValueHelp: function (sEntity, sTarget, oFilter, sTargetModelPath, onSuccess, sModel) {
			var aFinalFilter = Object.entries(oFilter || {}).map(function (aFilter) {
				var sFieldName = aFilter[0];
				var value = aFilter[1];
				if (typeof value === "object" && value.path) {
					return new Filter(value);
				}
				return new Filter({
					path: sFieldName,
					value1: value,
					operator: FilterOperator.EQ
				});
			});
			var oModel = this.getOwnerComponent().getModel(sModel);
			BusyIndicator.show();
			oModel.read(sEntity, {
				filters: aFinalFilter,
				success: function (oData, response) {
					if (sTarget) {
						this.setOwnerModelProperty(sTargetModelPath || "headersearch", sTarget, oData.results);
					}
					BusyIndicator.hide();
					if (typeof onSuccess === "function") {
						onSuccess(oData);
					}
				}.bind(this),
				error: function (oError) {
					BusyIndicator.hide();
				}
			});

		},

		handleProductDetail: function (oEvent) {
			var ProdId = oEvent.getSource().getBindingContext("oModel").getProperty("OrderedProd");
			var aFilter = [];
			if (ProdId) {
				aFilter.push(new Filter("ProductId", FilterOperator.EQ, ProdId));
			}
			if (!this._oDialog) {
				this._oDialog = sap.ui.xmlfragment("zcrm.zcrm_inquire_price.fragments.ProdDetail", this);
				this.getView().addDependent(this._oDialog);
				this._oDialog.open();
			} else {

				this._oDialog.open();
			}

			var oTable = sap.ui.getCore().byId("ProdDetail");
			var oBinding = oTable.getBinding("items");
			oBinding.filter(aFilter);

		},
		handleProdDetailCancel: function () {

			this._oDialog.close();
		},

		handlePopoverProd: function (oEvent) {
			var Img = oEvent.getSource().getSrc();
			this.getView().getModel("local").setData({
				Img: Img
			});
			if (!this._oPopover) {
				this._oPopover = sap.ui.xmlfragment("zcrm.zcrm_inquire_price.fragments.ProdImage", this);
				this.getView().addDependent(this._oPopover);
			}

			var oButton = oEvent.getSource();
			jQuery.sap.delayedCall(0, this, function () {
				this._oPopover.openBy(oButton);
			});
		},
		onProdValueHelp: function (oEvent) {
			var oHeadersearchData = this.getOwnerModelProperty("headersearch", "/");
			this.setOwnerModelProperty("headersearch", "/", {
				OdemeTipi: oHeadersearchData.OdemeTipi,
				oOdeme: oHeadersearchData.oOdeme
			});
			this.setOwnerModelProperty("search", "/", {});
			this.open("ProdSearch").setSource(oEvent.getSource());

		},
		onCustomerValueHelp: function (oEvent) {
			var oHeadersearchData = this.getOwnerModelProperty("headersearch", "/");
			this.setOwnerModelProperty("headersearch", "/", {
				OdemeTipi: oHeadersearchData.OdemeTipi,
				oOdeme: oHeadersearchData.oOdeme
			});
			this.setOwnerModelProperty("search", "/", {});
			this.open("CustomerSearch").setSource(oEvent.getSource());

		},
		onSelectFromCat: function (oEvent) {
			var oHeadersearchData = this.getOwnerModelProperty("headersearch", "/");
			this.setOwnerModelProperty("headersearch", "/", {
				OdemeTipi: oHeadersearchData.OdemeTipi,
				oOdeme: oHeadersearchData.oOdeme
			});
			this.setOwnerModelProperty("search", "/", {});
			var oSelectedData = this.getOwnerModelProperty("selected", "/");
			this.setOwnerModelProperty("selected", "/", {
				Custid: oSelectedData.Custid,
				Custidname: oSelectedData.Custidname,
				MusteriAdres: oSelectedData.Custidname
			});
			var oCode = "0020";
			// this.BaseValueHelp(oCode);
			this.BaseValueHelp("/valueHelpSet", "/KatalogTakim", {
				Code: oCode
			});
			this.open("selectFromCat").setSource(oEvent.getSource());

		},

		open: function (sFragmentName) {
			return this.getOwnerComponent().openFragment(sFragmentName, this.getView());
		},
		createActionResultDialog: function () {
			if (!this.oActionResults) {
				this.oActionResults = new Dialog({
					title: "Mesaj",
					content: new List({
						items: {
							path: "actionResultMessage>/",
							template: new NotificationListItem({
								// title: "{actionResultMessage>title}",
								description: "{actionResultMessage>message}",
								showCloseButton: false,
								priority: "{= {'error': 'High', 'warning': 'Medium', 'success': 'Low'}[${actionResultMessage>severity}] }"
							})
						}
					}),
					beginButton: new Button({
						text: "Close",
						press: function () {
							this.oActionResults.close();
						}.bind(this)
					})
				});
				this.getView().addDependent(this.oActionResults);
			}
		},

		showMessageBox: function (oData, type, onClose) {
			var list = [];

			if (typeof oData === "string") {
				list = [{
					severity: type || "success",
					message: oData
				}];
			} else if (Array.isArray(oData)) {
				list = oData;
			} else if (oData.responseText) {
				var message = oData.responseText;
				try {
					list = JSON.parse(message).error.innererror.errordetails;
				} catch (e) {
					list = [{
						code: "/Dogtas",
						message: "Bilinmeyen hata",
						severity: "error",
					}];
				}
			}

			if (list.length === 1) {
				var oMessage = list[0];
				var icons = {
					"error": MessageBox.Icon.ERROR,
					"warning": MessageBox.Icon.WARNING,
					"success": MessageBox.Icon.SUCCESS,
				};
				var titles = {
					"error": "Hata",
					"warning": "Uyarı",
					"success": "Başarılı",
				};
				MessageBox.show(oMessage.message, {
					icon: icons[oMessage.severity],
					title: titles[oMessage.severity],
					actions: [MessageBox.Action.OK],
					onClose: onClose
				});
			} else {
				this.setOwnerModelProperty("actionResultMessage", "/", list);
				this.getView().getController().oActionResults.open();
			}
		},
		onKarakteristik: function (oEvent) {
			var oContext = oEvent.getSource().getBindingContext("oConfig");
			var sProperty = oContext.getProperty();
			var sOrderedProd = sProperty.ZzorderedProd;
			var sclass = sProperty.Zzclass;
			var scharCode = sProperty.ZzcharCode;
			this.BaseValueHelp("/defaultValuesSet", /* targetproperty: */null, {
				Code:"0006",
				OrderedProd: sOrderedProd,
				Zzclass: sclass,
				ZzcharCode:scharCode
				
			}, /* targetmodel */null, function onSuccess(oData) {
				if (oData.results && oData.results.length > 0) {
					sProperty.ZzcharValue = oData.results[0].ZzcharValue;
					oContext.getModel().updateBindings();
				}
				
			}.bind(this));

			// ZzcharValue ZzcharValue 
		},
		onCreateSalesOrder: function (oEvent) {
			var oModel = this.getView().getModel("oModel");
			var oSource = oEvent.getSource();
			var sProcessType = oModel.getProperty("/ProcessType");
			
				var sFunctype = "002";
				this.BaseValueHelp("/processTypeSearchHelpSet", "/ProcessTypeModel", {
					FuncType: sFunctype
				}, undefined, function onSuccess(oData) {
				var iResultCount = 0;
				oData.results.forEach(function (item) {
					if (item.ProcessType) {
						iResultCount++;
					}
				});
				if (iResultCount > 1) {
					this.open("NewOrderType").setSource(oSource);
				}else{
					var FuncType =  oData.results[1].ProcessType;
					this.onFollowupItem(null, FuncType);
				}
				}.bind(this));
			

		},
		onFollowupItem: function (oEvent,sProcessType) {
			var oModel = this.getView().getModel("oModel");
			if(!sProcessType){
			var oText = sap.ui.getCore().byId("idOrderTypes").getSelectedButton().getText();
			sProcessType = oText.substr(oText.length - 4);
			}
			this.getView().getModel("selected").setProperty("/ProcessType",sProcessType);
			var sObjectId = oModel.getProperty("/ObjectId");
			this.BaseValueHelp("/followupItemPopupSet", "/FollowupItem", {
				ProcessType: sProcessType,
				ObjectId: sObjectId
			}, undefined, function onSuccess(oData) {
				this.open("NewOrderType").getDialog().close();
					this.open("FollowupItems");
				
			}.bind(this));
		},
	});
});